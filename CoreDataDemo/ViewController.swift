//
//  ViewController.swift
//  CoreDataDemo
//
//  Created by Kvana Inc 2 on 07/07/16.
//  Copyright © 2016 Kvana Inc 2. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    var modelData = [AlbumModel]()
@IBOutlet weak var coreTable: UITableView!
    var dataFromCore = [NSManagedObject] ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

 @IBAction func Save(sender:UIButton) {
        
        Alamofire.request(.GET, "http://jsonplaceholder.typicode.com/posts")
            .responseJSON { response in
                if let JSON = response.result.value {
                    for data in JSON as! [AnyObject]{
                        let details = AlbumModel(fromDictionary: data as! NSDictionary)
                       // self.modelData.append(details)
                        let appDelegate =
                            UIApplication.sharedApplication().delegate as! AppDelegate
                           let managedContext = appDelegate.managedObjectContext
                        
                        //2
                        let entity =  NSEntityDescription.entityForName("Albums",
                            inManagedObjectContext:managedContext)
                        
                        let person = NSManagedObject(entity: entity!,
                            insertIntoManagedObjectContext: managedContext)
                        
                        //3
                        person.setValue(details.title , forKey: "title")
                        //4
                        do {
                            try managedContext.save()
                            //5
                            self.dataFromCore.append(person)
                        } catch let error as NSError  {
                            print("Could not save \(error), \(error.userInfo)")
                        }

                        self.coreTable.reloadData()
                    }
                    print("JSON: \(JSON)")
                }
                
       }
        }
    
      override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    func tableView(tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return dataFromCore.count
       // return modelData.count
    }
    
    func tableView(tableView: UITableView,cellForRowAtIndexPath
        indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CoreTableViewCell", forIndexPath: indexPath) as! CoreTableViewCell
        cell.deleteBtn.tag = indexPath.row
       let album = dataFromCore[indexPath.row]
       cell.txtLbl.text = album.valueForKey("title") as? String
         //let modalArrayData = modelData[indexPath.row]
        //cell!.textLabel!.text = modalArrayData.title as? String
        return cell
    }

    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let alert = UIAlertController(title: "Update", message: "Please enter the new name for the album.", preferredStyle: .Alert)
        let updateAction = UIAlertAction(title: "Update", style: .Default){(_) in
            let nameTextField = alert.textFields![0]
            self.updateEntity(indexPath.row, newName: nameTextField.text!)
           self.coreTable.reloadData()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        alert.addTextFieldWithConfigurationHandler(nil)
         alert.addAction(updateAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
       
    }
    
    func updateEntity(index:Int, newName: String){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
      // dataFromCore[index].title = newName
        appDelegate.saveContext()
    }
    
      override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
       
        let fetchRequest = NSFetchRequest(entityName: "Albums")
       
        do {
            let results =
                try managedContext.executeFetchRequest(fetchRequest)
            dataFromCore = results as! [NSManagedObject]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    @IBAction func deleteBtnTapped(sender: UIButton) {
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        managedContext.deleteObject(dataFromCore[sender.tag])
        appDelegate.saveContext()
        dataFromCore.removeAtIndex(sender.tag)
        coreTable.reloadData()

    }
    
}

