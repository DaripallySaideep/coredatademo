//
//  AlbumModel.swift
//  CoreDataDemo
//
//  Created by Kvana Inc 2 on 07/07/16.
//  Copyright © 2016 Kvana Inc 2. All rights reserved.
//

import UIKit
import Foundation

class AlbumModel: NSObject {
   
    var body : String!
    var id : Int!
    var title : String!
    var userId : Int!
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        body = dictionary["body"] as? String
        id = dictionary["id"] as? Int
        title = dictionary["title"] as? String
        userId = dictionary["userId"] as? Int
    }
    
}
